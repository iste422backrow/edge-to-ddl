public enum ColumnType {

    VARCHAR,
    INTEGER,
    BOOLEAN,
    DOUBLE

}
