public class Database {

    private Table[] tables;
    private Field[] fields;


    public Table[] getTables() {
        return tables;
    }

    public void addTables(Table[] tables) {
        this.tables = tables;
    }

    public Field[] getFields() {
        return fields;
    }

    public void addFields(Field[] fields) {
        this.fields = fields;
    }
}
