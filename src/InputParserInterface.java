import java.io.File;

public interface InputParserInterface {

    void parseFile(File inputFile);

    Database getDatabase();

}
