import org.junit.Assert;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.instanceOf;
import java.io.*;

public class EdgeInputParserTest {

    @Test(expected=MalformedInputFormatException.class)
    public void test_for_malformed_edge_exception()
    {
        InputStreamReader isr = new InputStreamReader(this.getClass().getResourceAsStream("resources/bad-input.edge"));

        EdgeInputParser parser = new EdgeInputParser(isr);

        Database db = parser.getDatabase();
    }

    @org.junit.Test
    public void test_for_valid_edge_file()
    {
        InputStreamReader isr = new InputStreamReader(this.getClass().getResourceAsStream("resources/good-input.edge"));

        EdgeInputParser parser = new EdgeInputParser(isr);

        Assert.assertThat(parser.getDatabase(), instanceOf(Database.class));
    }

}
