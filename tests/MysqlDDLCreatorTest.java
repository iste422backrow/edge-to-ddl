import org.junit.Assert;
import org.junit.Test;

public class MysqlDDLCreatorTest {

    @Test
    public void test_mysql_generation_success(){
        Database valid_db = new Database();
        String expected_output = "";
        MysqlDDLCreator mysql_success = new MysqlDDLCreator(valid_db);
        Assert.assertEquals(mysql_success.generateDDL(), expected_output);
    }

    @Test(expected = InvalidDatabaseException.class)
    public void test_mysql_generation_fail(){
        Database invalid_db = new Database();
        MysqlDDLCreator mysql_fail = new MysqlDDLCreator(invalid_db);
    }
}
