import org.junit.Assert;
import org.junit.Test;

public class OracleDDLCreatorTest {

    @Test
    public void test_oracle_generation_success(){
        Database valid_db = new Database();
        String expected_output = "";
        MysqlDDLCreator oracle_success = new MysqlDDLCreator(valid_db);
        Assert.assertEquals(oracle_success.generateDDL(), expected_output);
    }

    @Test(expected = InvalidDatabaseException.class)
    public void test_oracle_generation_fail(){
        Database invalid_db = new Database();
        MysqlDDLCreator oracle_fail = new MysqlDDLCreator(invalid_db);
    }
}
