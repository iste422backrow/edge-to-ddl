import org.junit.Assert;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.instanceOf;
import java.io.InputStreamReader;

/**
 * Created by Ricky on 10/7/2016.
 */
public class RunConvertTest {

    @Test(expected=UnexpectedFiletypeException.class)
    public void test_for_unexpected_filetype_exception()
    {
        InputStreamReader isr = new InputStreamReader(this.getClass().getResourceAsStream("resources/bad-input.txt"));

        RunConvert runner = new RunConvert(isr);
    }

    @Test
    public void test_for_getting_edge_parser()
    {
        InputStreamReader isr = new InputStreamReader(this.getClass().getResourceAsStream("resources/bad-input.edge"));

        RunConvert runner = new RunConvert(isr);

        Assert.assertThat(runner.getParser(), instanceOf(EdgeInputParser.class));
    }

    @Test
    public void test_for_getting_xml_parser()
    {
        InputStreamReader isr = new InputStreamReader(this.getClass().getResourceAsStream("resources/bad-input.xml"));

        RunConvert runner = new RunConvert(isr);

        Assert.assertThat(runner.getParser(), instanceOf(XMLInputParser.class));
    }
}
