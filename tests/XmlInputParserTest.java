import org.junit.Assert;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.instanceOf;
import java.io.*;

public class XmlInputParserTest {

    @Test(expected=MalformedInputFormatException.class)
    public void test_for_malformed_xml_exception()
    {
        InputStreamReader isr = new InputStreamReader(this.getClass().getResourceAsStream("resources/bad-input.xml"));

        XMLInputParser parser = new XMLInputParser(isr);

        Database db = parser.getDatabase();
    }

    @org.junit.Test
    public void test_for_valid_xml_file()
    {
        InputStreamReader isr = new InputStreamReader(this.getClass().getResourceAsStream("resources/good-input.xml"));

        XMLInputParser parser = new XMLInputParser(isr);

        Assert.assertThat(parser.getDatabase(), instanceOf(Database.class));
    }
}
